---
title: "Introduction to ggpubr"
date: "5/18/2018"
output:
  ioslides_presentation:
    smaller: true
    fig_width: 4
    fig_height: 3
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## ggpubr: An R package for Simplifying Creation of High-Quality Chart

* ggpubr aims to help create "publication-ready" plots
* Based on ggplot2
* Simplified "one-command" usage
* More pleasing default font sizes and theme
* Installation from within R: `install.packages("ggpubr")`


## Preliminaries

```{r warning=FALSE}
# Load data
library(ggpubr)
data("ToothGrowth") # not really needed, already preloaded
a <- ToothGrowth
head(a, 4)
```

## Boxplot Example

* Instead of `ggplot(...) + geom_boxplot()` use of one `ggboxplot` command
* from: <http://www.sthda.com/english/rpkgs/ggpubr/>

```{r}
p <- ggboxplot(a, x = "dose", y = "len")
print(p)
```

## Customizing ggpubr Plots

* Added jitter points
* Change outline colors and point shape via variable dose
* Use of custom color palette

```{r}
q <- ggboxplot(a, x = "dose", y = "len", xlab="Dose", ylab="Length",
                color = "dose", palette =rainbow(3),
                add = "jitter", shape = "dose")
print(q)
```

## A Violin Plot

* Command ggviolin for simplied violin plots

```{r}
v <- ggviolin(a, x = "dose", y = "len", xlab="Dose", ylab="Length",
                color = "dose", palette =rainbow(3))
print(v)
```

## A Density Plot

* Command ggdensity for simplied violin plots

```{r}
a$Dose <- as.character(a$dose)
w <- ggdensity(a, "len", xlab="Length",
                color="Dose", fill = "Dose")
print(w)
```


## Exercise: Customizing ggpubr Plots

* Attempt to reproduce the boxplot example. 
* Customize color palettes
* The font of the axis tick labels can be changed with parameter `font.x` and `font.y`. The parameter needs to be a vector or list of length 3 with font-size, style ("plain" or "bold" or "italic") and the color. Example: `font.x= c(20,"plain", "black")` Try out different variations for x and y axis in the `ggboxplot` command.

## Arranging Plots with ggarrange

* Plots can be arrange in a panel with `ggarrange`
* Specify rows and columns with parameters nrows and ncols

```{r}
a <- ggarrange(q,w,nrow = 1, ncol=2)
```

## ggarrange (continued)

```{r}
print(a)
```

